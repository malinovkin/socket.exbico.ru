var app = require('express')();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});

var https = require('https');
var fs = require('fs');
var options = {
    key:    fs.readFileSync('/mnt/sites/socket.exbico.ru/certs/socket.exbico.ru18.key'),
    cert:   fs.readFileSync('/mnt/sites/socket.exbico.ru/certs/socket_exbico_ru18.crt'),
    ca:     fs.readFileSync('/mnt/sites/socket.exbico.ru/certs/COMODORSADomainValidationSecureServerCA.crt'),
    requestCert: false,
    rejectUnauthorized: false

};

var app = https.createServer(options);
var sio = require('socket.io');
var io = sio.listen(app,{key:options.privateKey,cert:options.cert,ca:options.ca, origins: '*:*'});
//var io = require('socket.io')(app);



io.on('connection', function(socket){

    console.log('user connected');

    if(typeof socket.handshake.query.room != typeof undefined) {
        socket.join(socket.handshake.query.room);
    }
    
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('report', function (data) {
        console.log(data.roomId);
        io.to(data.roomId).emit('report', data.reportId);
    });

    socket.on('newComment', function(data){
        console.log(data.classId);
        console.log(data.objectId);
        io.to(data.roomId).emit('newComment', data.classId, data.objectId);
    });
});

app.listen(3000, function(){
    console.log('listening on *:443');
});
